<?php
$title = "EcoLibros";
$serverUrl = "";
?>
<!DOCTYPE html>
<html lang="es" itemscope itemtype="https://schema.org/">
<head>
  <meta charset="UTF-8">
  <title><?php echo $title;?></title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta content="yes" name="apple-mobile-web-app-capable">
  <link rel="canonical" href="http://ecolibros.cl" />

  <!-- Icons -->
  <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/manifest.json">
  <meta name="msapplication-TileColor" content="#00acd2">
  <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  <meta name="theme-color" content="#00acd2">

  <!-- Search Engines -->
  <meta name="author" content="Doonde">
  <meta name="description" content="<?php echo $share_content;?>">
  <meta name="HandheldFriendly" content="True" />
  <meta name="MobileOptimized" content="320" />
  <link href="humans.txt" rel="author">

  <!-- Twitter Cards -->
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="@Ecolibros">
  <meta name="twitter:creator" content="@Ecolibros">
  <meta name="twitter:description" content="<?php echo $share_content;?> en Ecolibros.cl">
  <meta name="twitter:title" content="<?php echo $share_content;?>">
  <meta name="twitter:image" content="<?php echo $share_image;?>">

  <!-- Facebook Open Graph -->
  <meta property="og:site_name" content="Ecolibros">
  <meta property="og:type" content="website">
  <meta property="og:url" content="<?php echo $share_url;?>">
  <meta property="og:description" content="Ecolibros">
  <meta property="og:title" content="<?php echo $share_content;?>">
  <meta property="og:image" content="<?php echo $share_image;?>">
  <meta property="fb:admins" content="ldc69,100000214947800">

  <!-- Google + -->
  <meta itemprop="name" content="Ecolibros">
  <meta itemprop="description" content="<?php echo $share_content;?>">
  <meta itemprop="image" content="<?php echo $share_image; ?>">


  <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <script src="js/platform.js"></script>
  <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-557cd14f0ae1eb09" async="async"></script>


  <link rel="stylesheet" href="css/style.min.css">
  <link rel="stylesheet" href="css/bootstrap-social.css">
  <script>
    var ServiceUrl = "<?php echo $serverUrl;?>";
    var user_id_facebook = '';
    </script>
  </head>
<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-74368042-1', 'auto');
  ga('send', 'pageview');

</script>

  <?php include('views/header.php');
?>


<?php include('views/maps.php'); ?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCd4LS0T_Otappn8O9mKYKvGAgWLzuWuSo&v=3&libraries=places&callback=onGoogleReady"></script>

  <script type="text/javascript">


    function loadScript() {


    }
    function loadVars(){


    }

    function addPostVar(name, value, form){
      var input = document.createElement("input");
      input.setAttribute("type", "hidden");
      input.setAttribute("name", name);
      input.setAttribute("value",value);
      document.getElementById(form).appendChild(input);
    }
    


    window.onload = loadScript();
    document.ready = loadVars();


  </script>
   <script>
        function truncate(text, length, end) {
          try {
            var rr =  (isNaN(length) && (length = 10), void 0 === end && (end = "..."), text.length <= length || text.length - end.length <= length ? text : String(text).substring(0, length - end.length) + end)
            return decodeURIComponent(escape(rr));
          }
          catch(err){
        //console.log(err, text);
        return text;
      }
    }
    function clearText(text){
      try{
        return decodeURIComponent(escape(""+text))
      }catch(err){
        //console.log(err)
        return text;
      }
    }
    function reformatUrl(text, end) {
      try {
        return text.indexOf("http://") >= 0 ? text: "http://doondeapp.com/admin/"+text;            
      }
      catch(err){
        //console.log(err);
        return text;
      }
    }
    var QueryString = function () {
      // This function is anonymous, is executed immediately and 
      // the return value is assigned to QueryString!
      var query_string = {};
      var query = window.location.search.substring(1);
      var vars = query.split("&");
      for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
            // If first entry with this name
            if (typeof query_string[pair[0]] === "undefined") {
              query_string[pair[0]] = pair[1];
            // If second entry with this name
          } else if (typeof query_string[pair[0]] === "string") {
            var arr = [ query_string[pair[0]], pair[1] ];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
          } else {
            query_string[pair[0]].push(pair[1]);
          }
        } 
        return query_string;
      } ();

    </script>
</body>
</html>