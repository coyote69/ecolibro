<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Iniciar Sesión</h4>
      </div>
      <div class="modal-body">
      	<strong>Al iniciar sesión en Doonde podrás:</strong><br>
      	<ul>
      	<li>Acceder a la promociones exclusivas.</li>
      	<li>Revisar tus promociones en cualquier momento</li>
      	</ul>
      	<hr>
        <a class="btn btn-social btn-facebook" onclick="fb_login();">
          <i class="fa fa-facebook"></i>
          Iniciar con Facebook
        </a>
	

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="questmail" role="dialog" aria-labelledby="promocion_titulo" aria-hidden="true">
  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i style="font-size: 35px;" class="fa fa-times-circle"></i></span></button>
        <h3 class="modal-title" id="promocion_titulo">Tu email no está registrado</h3>
      </div>
      <div class="modal-body">
        <h4>Necesitamos tu email para enviarte la información de esta promoción</h4>
        <div class="row">
        <div class="col-lg-9">
          <input type="text" id="myEmail" class="form-control input-lg" name="myEmail" placeholder="tuemail@correo.com">
        </div>
        <div class="col-lg-3">
          <div id="btnThisIsMyEmail" class="btn btn-success btn-lg">Continuar</div>
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

