<style>
  .promo_image{
    width: 100%;
    max-width: 320px;
    border-radius: 4px;
  }
  .promo_precio{
    font-family: 'Lato';
    font-weight: 600;
    color: #1dcc6b;
  }
  .my-strong{
      font-weight: bold;
  font-size: 12px;
  }
  #negocio_telefono{
    font-family: "Roboto" !important;
font-size: 18px !important;
font-weight: 300;
padding: 0px;
margin: 4px;
  }
  #da_reply_form .input-row{
    margin: 3px;
  }
  #negocio_nombre{
    font-size: 20px;
    font-weight: 700 !important;
}
</style>
<div class="container-fluid">
        <h2 class="promo_precio" id="promo_precio">$...clp</h2>

  <div class="row">
  <div class="col-md-offset-1 col-md-4 " style="margin-right: 15px;">
  <div class="row hidden-xs hidden-sm">
    <img class="promo_image preview panel" id="promo_imagen" src="">
    </div>
   <div class="row">
        <div id="promo_texto">
         
        </div>
      </div>
      </div>

    <div class="col-md-6">
    <div class="row">
      <h4>Estado del libro: <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half"></i></h4>
      <h4>Sentimiento del libro: Alegria</h4>
            

    </div>
      <div class="row">
        <div  id="adquirirPromocion" class="btn btn-lg btn-primary" style="font-size: 15px; height: 25;">
          <i id="waitQuiero" class="fa fa-book"></i>Ver biblioteca de <strong id="user_name"></strong><br>
        </div>

        <br>
        <span style="font-size: 12px;" id="promo_id">...</span><br>
        <div class="addthis_sharing_toolbox"></div>

        <hr>
      </div>
     
      <div class="row">
      <h4>Envíale un mensaje</h4>
      <form id="da_reply_form" >
      
          <div class="input-row">
            
            <input id="your_name" class="form-control icons-inside" name="name" size="35" maxlength="50" tabindex="1" value="" placeholder="Tu nombre" type="text">
          
          </div>

          <div class="input-row">
            <input id="user_email" class="form-control icons-inside" name="email" size="35" maxlength="80" tabindex="2" value="" placeholder="Tu e-mail" type="email">
           </div>

          <div class="input-row">
            <input id="phone" class="form-control icons-inside" name="phone" size="20" maxlength="11" tabindex="3" value="" placeholder="Tu teléfono (Opcional)" type="tel">
           </div>

          <div class="input-row">
            <textarea style="height: 33px;" id="adreply_body" class="form-control icons-inside" id="message" cols="37" rows="8" tabindex="4" placeholder="Mensaje" maxlength="3000" wrap="hard"></textarea>
          </div>
      <h2 id="message_sended" style="display: none;"><i class="fa fa-thumbs-o-up"></i> Mensaje enviado</h2>
      <div class="input-row"> 
          <div class="btn btn-default " id="send"  onclick="send_message();">Enviar Mensaje</div>     
      </div>
    </form>
      </div>
      <br>
      <div class="row">
        <div class="preview panel panel-default" >
          <div class="panel-footer">

            <div class="media">
              <div class="pull-left">
                <a class="negocio_link">
                  <img class="img-rounded" id="negocio_logo" src="" >
                </a>
              </div>
              <div class="media-body ng-binding" style="height:auto;">
                  <a class="negocio_link">
                    <strong id="negocio_nombre"></strong> <br><small>Ver biblioteca</small>
                  </a>
                 <br/>
              </div>
            </div>
            <br><br>
            <a class="btn btn-primary" id="negocio_telefono_btn" href="" > 
              <strong id="negocio_telefono">...</strong>
            </a>
            <br><br>
            <a href="" target="_blank" id="negocio_direccion">...</a>

            <img style="
    width: 100%;
    height: 100%;
" src="#" id="map_image">
          </div>
        </div>
      </div>
    </div>
  </div>  
</div>